package com.revolut.appium.tests;

import com.revolut.appium.utils.BankAccountType;
import com.revolut.appium.utils.BankDataStorage;
import com.revolut.appium.activities.WalletActivity;

import org.testng.annotations.Test;

import static com.revolut.appium.tests.TestSetup.app;
import static com.revolut.appium.tests.TestSetup.log;

public class RegressionAddNewBeneficiaryTest {

    // One bug: if swiped, then I cannot choose menu point option

    @Test(groups={"beneficiary"})
    public void testAddSelf() throws InterruptedException, NoSuchFieldException, IllegalAccessException {

        log.info("Begins testing of login page...");

        app.login()
                .skipIfTutorialAppeared(2)
                .setPhoneField("662266")
                .tapSignIn()
                .enterPassCode("1111")
                .waitForActivity(WalletActivity.ACTIVITY_NAME, 10);

        app.wallet()
                .tapTransferBtn()
                .tapToBankAccount()
                .skipIfTutorialAppeared(2);

        app.bank()
                .addAccount()
                .chooseType(BankAccountType.MYSELF)
                .setCountryFromList("France")
                .setCurrencyFromList("USD")
                .clickNext()
                .setLegalFirstName("John")
                .setLegalLastName("Doe")
                .setIbanId(BankDataStorage.getIban())
                .setBicSwiftId(BankDataStorage.getBicSwift())
                .setMobilePhone("+79164950985")
                .setEmail("alexander.momot@gmail.com")
                .clickNext();
//
//        assertTrue(app.bank().getStatusText().contains("John"));
//
//        app.bank()
//                .clickDone();
//
//        app.bank()
//                .deleteAccount();


        Thread.sleep(1);
    }

//    @Test(groups = {"beneficiary"})
//    public void testAddAnotherPerson() {
//
//    }
//
//    @Test(groups = {"beneficiary"})
//    public void testAddBusinessAccount() {
//
//    }
//
//
//
//    @Test(groups = {"beneficiary"})
//    public void testAddRussianBank() {
//
//    }
//
//    @Test(groups = {"beneficiary"})
//    public void testAddEuBank() {
//
//    }
}