package com.revolut.appium.activities;

@SuppressWarnings("WeakerAccess")
public interface Activity {

    String activityName = "";

    default String getActivityName() throws NoSuchFieldException, IllegalAccessException {
        return activityName;
    }

}
