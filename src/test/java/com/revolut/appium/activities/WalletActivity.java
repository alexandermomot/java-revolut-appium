package com.revolut.appium.activities;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

@SuppressWarnings("WeakerAccess")
public class WalletActivity extends BaseActivity {

    public static String ACTIVITY_NAME = "com.revolut.ui.wallet.activity.WalletActivity";

    @AndroidFindBy(id = GLOBAL_NAMESPACE + "/button_top_up")
    private AndroidElement btnTopUp;

    @AndroidFindBy(id = GLOBAL_NAMESPACE + "/button_transfer")
    private AndroidElement btnTransfer;

    @AndroidFindBy(id = GLOBAL_NAMESPACE + "/dialogplus_list")
    private AndroidElement paneTransferOptions;

    @AndroidFindBy(xpath = "//android.widget.RelativeLayout[4]")
    private AndroidElement btnToBankAccount;

    public WalletActivity(AndroidDriver driver) {
        super(driver);
    }

    public WalletActivity tapTopUpBtn() throws InterruptedException {
        btnTopUp.click();
        return this;
    }

    public WalletActivity tapTransferBtn() {
        btnTransfer.click();
        waitForElementVisible(paneTransferOptions, 5);
        return this;
    }

    public BankAccountTransferActivity tapToBankAccount() {
        btnToBankAccount.click();
        return new BankAccountTransferActivity(driver);
    }


}
