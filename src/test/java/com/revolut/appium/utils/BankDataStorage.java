package com.revolut.appium.utils;

public class BankDataStorage {

    private static final BankDataStorage ourInstance = new BankDataStorage();

    private BankDataStorage() {}

    public static BankDataStorage getInstance() {
        return ourInstance;
    }

    public static String getBicSwift() {
        return "BBVAFRPP";
    }

    public static String getIban() {
        return "FR1420041010050500013M02606";
    }


}
