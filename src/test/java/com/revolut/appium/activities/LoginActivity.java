package com.revolut.appium.activities;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

@SuppressWarnings("WeakerAccess")
public class LoginActivity extends BaseActivity<LoginActivity> {

    public static String ACTIVITY_NAME = "com.revolut.ui.login.SignPhoneNumberActivity";

    static String ACTIVITY_PASS_NAME = "com.revolut.ui.login.pin.LoginActivity";

    @AndroidFindBy(id = GLOBAL_NAMESPACE + "/uic_edit_phone_number")
    private AndroidElement inputPhoneNumber;

    @AndroidFindBy(id = GLOBAL_NAMESPACE + "/signup_next")
    private AndroidElement btnSignUp;

    @AndroidFindBy(id = GLOBAL_NAMESPACE + "/header_next")
    private AndroidElement hdrNext;

    @AndroidFindBy(id = GLOBAL_NAMESPACE + "/pincode_widget")
    private AndroidElement panePincode;

    public LoginActivity(AndroidDriver driver) {
        super(driver);
    }

    public LoginActivity setPhoneField(String phoneNumber) {
        inputPhoneNumber.click();
        inputPhoneNumber.sendKeys(phoneNumber);
        return this;
    }

    public LoginActivity tapSignIn() {
        btnSignUp.click();
        waitForElementVisible(panePincode);
        return this;
    }

    public LoginActivity enterPassCode(String pass) throws InterruptedException {
        String keyBrdMutator = "/button_%";
        for (char s: pass.toCharArray()) {
            this.findById(keyBrdMutator.replaceAll("%", String.valueOf(s))).click();
        }
        return this;
    }

}