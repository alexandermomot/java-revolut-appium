package com.revolut.appium.activities;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

import java.util.List;
import java.util.NoSuchElementException;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

@SuppressWarnings("WeakerAccess")
public abstract class BaseActivity<T> implements Activity {

    AndroidDriver driver;

    Dimension size;

    int leftSwipeCoord;

    int rightSwipeCoord;

    static final String ACTIVITY_TUTORIAL = "com.revolut.ui.tutorial.TutorialActivity";

    static final String GLOBAL_NAMESPACE = "com.revolut.revolut.test:id";

    @AndroidFindBy(id = GLOBAL_NAMESPACE + "/tutorial_image")
    private AndroidElement imgTutorial;

    @AndroidFindBy(id = GLOBAL_NAMESPACE + "/header_next")
    private AndroidElement btnSkip;

    protected BaseActivity(AndroidDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);

        size = driver.manage().window().getSize();

        this.leftSwipeCoord = (int) (size.width * 0.20);
        this.rightSwipeCoord = (int) (size.width * 0.80);
    }

    public String getActivityName()
            throws NoSuchFieldException, IllegalAccessException {
        return (String) getClass().getDeclaredField("ACTIVITY_NAME").get(null);
    }

    AndroidElement waitForElementVisible(AndroidElement element) {
        return (AndroidElement) new WebDriverWait(driver, 15)
                .withMessage("Element never becomes visible")
                .until(visibilityOf(element));
    }

    AndroidElement waitForElementVisible(AndroidElement element, int timeout) {
        return (AndroidElement) new WebDriverWait(driver, timeout)
                .withMessage("Element never becomes visible")
                .until(visibilityOf(element));
    }

    public T skipIfTutorialAppeared(int secs)
            throws InterruptedException, NoSuchFieldException, IllegalAccessException {
        if (isTutorialAppeared(secs)) {
            btnSkip.click();
            System.out.println("Current activity: " + driver.currentActivity());
            waitForActivity(this.getActivityName(), 5);
        }
        return (T) getClass().cast(this);
    }

    public T swipeLeftForOptions(AndroidElement el) {
        TouchAction action = new TouchAction(driver);
        String haha = el.getText();
        action.longPress(el).moveTo(leftSwipeCoord, el.getCenter().y).release().perform();
        return (T) getClass().cast(this);
    }

    protected boolean isTutorialAppeared(int secs) {
        boolean state = true;
        try {
            waitForActivity(ACTIVITY_TUTORIAL, secs);
            waitForElementVisible(imgTutorial, secs);
        } catch (TimeoutException | InterruptedException e) {
            state = false;
        }
        return state;
    }

    public void waitForActivity(String activity, int timeout) throws InterruptedException {
        long startTime = System.currentTimeMillis();
        while (System.currentTimeMillis() - startTime < timeout * 1000) {
            if (driver.currentActivity().equals(activity)) return;
            Thread.sleep(500);
        }
        throw new TimeoutException("Activity " + activity + " timeout exception");
    }

    protected AndroidElement findById(String id) {
        return (AndroidElement) driver.findElementById(GLOBAL_NAMESPACE + id);
    }

    protected AndroidElement findByText(List<AndroidElement> Elements, String text) {
        for (AndroidElement element: Elements) {
            if (element.getText().equals(text)) {
                return element;
            }
        }
        throw new NoSuchElementException("Element with text " + text + " wasn't found");
    }

    protected void takeScreenShot() {
        driver.getScreenshotAs(OutputType.BASE64);
    }

}