package com.revolut.appium.utils;

public enum BankAccountType {

    MYSELF, ANOTHER_PERSON, BUSINESS

}
