package com.revolut.appium;

import com.revolut.appium.activities.BankAccountTransferActivity;
import com.revolut.appium.activities.BaseActivity;
import com.revolut.appium.activities.LoginActivity;
import com.revolut.appium.activities.WalletActivity;

import io.appium.java_client.android.AndroidDriver;

public class RevolutApp extends BaseActivity {

    private final AndroidDriver driver;

    public RevolutApp(AndroidDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public AndroidDriver getDriver() {
        return this.driver;
    }

    public LoginActivity login() {
        return new LoginActivity(driver);
    }

    public WalletActivity wallet() {
        return new WalletActivity(driver);
    }

    public BankAccountTransferActivity bank() {
        return new BankAccountTransferActivity(driver);
    }

}