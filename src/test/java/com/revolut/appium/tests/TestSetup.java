package com.revolut.appium.tests;

import com.github.javafaker.Faker;
import com.revolut.appium.RevolutApp;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.log4testng.Logger;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;

public class TestSetup {

    static Logger log;
    static RevolutApp app;

    private AndroidDriver<org.openqa.selenium.WebElement> driver;
    private DesiredCapabilities capabilities;

    private static final
    String APP_RELATIVE_PATH =  "/src/app/binary";

    private static final
    String APP_ACTIVITY = "com.revolut.ui.tutorial.TutorialActivity";

    private static final
    String AVD_NAME = "pixel";

    @BeforeTest
    @Parameters({ "device-name", "apk-file-name" })
    public void prepareTestingEnvironment(
            String deviceName,
            @Optional("Revolut_qa_4.3.0.237.apk") String apkFileName) throws URISyntaxException {
        log = Logger.getLogger(TestSetup.class);
        String appAbsPath = System.getProperty("user.dir") + APP_RELATIVE_PATH + "/" + apkFileName;
        capabilities = new DesiredCapabilities() {{
            setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
            setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
            setCapability(MobileCapabilityType.VERSION, "6.0.1");
            setCapability(MobileCapabilityType.APP, appAbsPath);
            setCapability(MobileCapabilityType.ORIENTATION, "PORTRAIT");
            setCapability(AndroidMobileCapabilityType.AVD, AVD_NAME);
            setCapability(AndroidMobileCapabilityType.APP_WAIT_ACTIVITY, APP_ACTIVITY);
            setCapability(AndroidMobileCapabilityType.APP_WAIT_DURATION, "30000");
            setCapability(AndroidMobileCapabilityType.DEVICE_READY_TIMEOUT, "120");
        }};
    }

    @BeforeTest
    public void setUpApplication() throws MalformedURLException {
        log.info("Set up started...");
        driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        app = new RevolutApp(driver);
    }

    @AfterTest
    public void tearDownApplication() {
        driver.closeApp();
        driver.quit();
    }
}

