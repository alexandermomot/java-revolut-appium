package com.revolut.appium.activities;

import com.revolut.appium.utils.BankAccountType;

import java.util.List;
import org.openqa.selenium.NoSuchElementException;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

@SuppressWarnings("WeakerAccess")
public class BankAccountTransferActivity extends BaseActivity {

    public static String ACTIVITY_NAME = "com.revolut.ui.bank.BankTransferActivity";

    // Shared buttons between instances

    @AndroidFindBy(id = GLOBAL_NAMESPACE + "/button_next")
    private AndroidElement btnNext;

    // Group of elements on the beneficiaries screen

    @AndroidFindBy(id = GLOBAL_NAMESPACE + "/add_beneficiary_icon")
    private AndroidElement btnAddNewBeneficiaryFromScratch;

    @AndroidFindBy(id = GLOBAL_NAMESPACE + "/list_add_new_item_text")
    private AndroidElement btnAddNewBeneficiaryInList;

    @AndroidFindBy(id = GLOBAL_NAMESPACE + "/swipe_layout")
    private List<AndroidElement> menuItemBeneficiary;

    // Group of operation state screen elements

    @AndroidFindBy(id = GLOBAL_NAMESPACE + "/operation_state_icon")
    private AndroidElement stateIcon;

    @AndroidFindBy(id = GLOBAL_NAMESPACE + "/operation_state_title")
    private AndroidElement stateTitle;

    @AndroidFindBy(id = GLOBAL_NAMESPACE + "/operation_status_button")
    private AndroidElement btnDone;


    public BankAccountTransferActivity(AndroidDriver driver) {
        super(driver);
    }

    public BankAccountTransferActivity clickNext() {
        btnNext.click();
        return this;
    }

    /**
     * This will click on the button with picture, otherwise to the
     * button on the top of the list
     *
     * @return BankAccountTransferActivity
     */
    public BankAccountTransferActivity addAccount() {
        AndroidElement el = btnAddNewBeneficiaryFromScratch;
        try {
            el.isDisplayed();
        } catch (NoSuchElementException e) {
            el = btnAddNewBeneficiaryInList;
        }
        el.click();
        return this;
    }

    /**
     * This will swipe out the section in one account and tap del
     *
     * @return BankAccountTransferActivity
     */
    public BankAccountTransferActivity deleteAccount() {
        this.swipeLeftForOptions(menuItemBeneficiary.get(0));
        return this;
    }

    /**
     * This will swipe out the section in one account and tap edit
     *
     * @return BankAccountTransferActivity
     */
    public BankAccountTransferActivity editAccount() {
        return this;
    }

    class BankAccountCreateStep1 {

        // Group of elements with the transfer type (my self, another person, business)

        @AndroidFindBy(id = GLOBAL_NAMESPACE + "/item_name")
        private List<AndroidElement> radioButtonsToWho;

        public BankAccountCreateStep1 chooseType(BankAccountType num) {
            switch (num) {
                case MYSELF:
                    findByText(radioButtonsToWho, "To myself").click();
                    break;
                case ANOTHER_PERSON:
                    findByText(radioButtonsToWho, "To another person").click();
                    break;
                case BUSINESS:
                    findByText(radioButtonsToWho, "To a business").click();
                    break;
            }
            return this;
        }

        public BankAccountCreateStep1 chooseType(String num) {
            switch (num) {
                case "myself":
                    findByText(radioButtonsToWho, "To myself").click();
                    break;
                case "another person":
                    findByText(radioButtonsToWho, "To another person").click();
                    break;
                case "business":
                    findByText(radioButtonsToWho, "To a business").click();
                    break;
            }
            return this;
        }
    }

    class BankAccountCreateStep2 {

        // Group of elements with country and currency

        @AndroidFindBy(id = GLOBAL_NAMESPACE + "/edit_country")
        private AndroidElement selectCountryBtn;

        @AndroidFindBy(id = GLOBAL_NAMESPACE + "/edit_currency")
        private AndroidElement selectCurrencyBtn;

        @AndroidFindBy(id = GLOBAL_NAMESPACE + "/search_src_text")
        private AndroidElement inputSearchText;

        @AndroidFindBy(id = GLOBAL_NAMESPACE + "/text_view_currency_description")
        private AndroidElement optionCurrencyElement;

        @AndroidFindBy(id = GLOBAL_NAMESPACE + "/country_item")
        private AndroidElement optionCountryElement;

        public BankAccountCreateStep2 setCountryFromList(String country) {
            selectCountryBtn.click();
            inputSearchText.sendKeys(country);
            optionCountryElement.click();
            return this;
        }

        public BankAccountCreateStep2 setCurrencyFromList(String currency) {
            selectCurrencyBtn.click();
            inputSearchText.sendKeys(currency);
            optionCurrencyElement.click();
            return this;
        }
    }

    class BankAccountCreateStep3 {

        // Group of elements with the account details of beneficiary

        @AndroidFindBy(id = GLOBAL_NAMESPACE + "/first_name")
        private AndroidElement inputFirstName;

        @AndroidFindBy(id = GLOBAL_NAMESPACE + "/last_name")
        private AndroidElement inputLastName;

        @AndroidFindBy(id = GLOBAL_NAMESPACE + "/server_field_0")
        private AndroidElement inputIban;

        @AndroidFindBy(id = GLOBAL_NAMESPACE + "/server_field_1")
        private AndroidElement inputBicSwift;

        @AndroidFindBy(id = GLOBAL_NAMESPACE + "/mobile_phone")
        private AndroidElement inputMobilePhone;

        @AndroidFindBy(id = GLOBAL_NAMESPACE + "/email")
        private AndroidElement inputEmail;

        public BankAccountCreateStep3 setLegalFirstName(String name) {
            inputFirstName.sendKeys(name);
            return this;
        }

        public BankAccountCreateStep3 setLegalLastName(String name) {
            inputLastName.sendKeys(name);
            return this;
        }

        public BankAccountCreateStep3 setIbanId(String id) {
            inputIban.sendKeys(id);
            return this;
        }

        public BankAccountCreateStep3 setBicSwiftId(String id) {
            inputBicSwift.sendKeys(id);
            return this;
        }

        public BankAccountCreateStep3 setMobilePhone(String phone) {
            inputMobilePhone.sendKeys(phone);
            return this;
        }

        public BankAccountCreateStep3 setEmail(String email) {
            inputEmail.sendKeys(email);
            return this;
        }
    }

    class BankAccountCreateStep4 {

        // Group of elements with the beneficiary address

    }

    class BankAccountCreateFinalStep {

        // Group of elements with the beneficiary address

        public String getStatusText() {
            waitForElementVisible(stateTitle);
            return stateTitle.getText();
        }

        public BankAccountTransferActivity clickDone() throws InterruptedException {
            waitForElementVisible(btnDone);
            btnDone.click();
            waitForActivity(ACTIVITY_NAME, 10);
            return new BankAccountTransferActivity(driver);
        }
    }

}



